import { StatusBar } from "expo-status-bar";
import React from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";

import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import Navigation from "./navigation";
import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyB6WR20VzTMGjDn8y4aD31mfGh432x6Un8",
  authDomain: "hello-firebase-auth-3f4e5.firebaseapp.com",
  databaseURL:
    "https://hello-firebase-auth-3f4e5-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "hello-firebase-auth-3f4e5",
  storageBucket: "hello-firebase-auth-3f4e5.appspot.com",
  messagingSenderId: "867513869133",
  appId: "1:867513869133:web:b3404cb314532c0cf34d04",
  measurementId: "G-71G8SV2HNX",
};

//initialize firebase
firebase.initializeApp(firebaseConfig);

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}
