import * as Font from "expo-font";

export default useFonts = async () =>
  await Font.loadAsync({
    "Prompt-Black": require("./assets/fonts/Prompt-Black.ttf"),
    "Prompt-BlackItalic": require("./assets/fonts/Prompt-BlackItalic.ttf"),
  });
