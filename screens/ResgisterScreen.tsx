import * as React from "react";
import {
  StyleSheet,
  TextInput,
  ScrollView,
  Button,
  SafeAreaView,
  ImageBackground,
  TouchableHighlight,
  TouchableOpacity,
} from "react-native";

import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import firebase from "firebase";
import styles from "../Style";

export default function TabOneScreen({
  navigation,
}: RootTabScreenProps<"TabOne">) {
  const [userName, onChangeTextUsername] = React.useState("");
  const [email, onChangeTextEmail] = React.useState("");
  const [firstName, onChangeTextFirstName] = React.useState("");
  const [lastName, onChangeTextLastName] = React.useState("");
  const [address, onChangeTextAddress] = React.useState("");
  const [phone, onChangeTextPhone] = React.useState("");
  const [gender, onChangeTextGender] = React.useState("");
  const [password, onChangeTextPassword] = React.useState("");
  const onPressBack = () => {
    navigation.goBack();
  };
  const onPressRegsiger = () => {
    // navigation.navigate("Login");
    console.log(
      userName,
      email,
      firstName,
      lastName,
      address,
      phone,
      gender,
      password
    );
    try {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then((res) => {
          firebase
            .database()
            .ref("users/" + res.user.uid)
            .set({
              userName,
              email,
              firstName,
              lastName,
              address,
              phone,
              gender,
              password,
            });
        });
    } catch (error) {
      alert(error);
    }
  };
  const image = require("../assets/images/Asset22-100.jpg");
  const [radioSelected, radioClick] = React.useState("");
  // const radioClick = (click = { id: 1 }) => {
  //   radioSelected = click;
  //   console.log(click);
  // };

  const products = [
    {
      id: 1,
    },
    {
      id: 2,
    },
  ];

  return (
    <ImageBackground source={image} resizeMode="cover" style={styles.image}>
      <ScrollView>
        <View style={styles.modalheader}>
          <Text style={styles.modalheadertitle}>สมัครสมาชิก</Text>
        </View>
        <View style={styles.textInput}>
          <Text style={styles.subtitle}>ชื่อผู้ใช้</Text>
          <TextInput
            style={{
              height: 40,
              width: "55%",
              borderColor: "gray",
              borderWidth: 1,
              borderRadius: 15,
              backgroundColor: "#fff",
            }}
            onChangeText={(text) => onChangeTextUsername(text)}
            value={userName}
          />
        </View>
        <View style={styles.textInput}>
          <Text style={styles.subtitle}>รหัสผ่าน</Text>
          <TextInput
            style={{
              height: 40,
              width: "55%",
              borderColor: "gray",
              borderWidth: 1,
              borderRadius: 15,
              backgroundColor: "#fff",
            }}
            onChangeText={(text) => onChangeTextPassword(text)}
            value={password}
          />
        </View>
        <View style={styles.textInput}>
          <Text style={styles.subtitle}>ชื่อ</Text>
          <TextInput
            style={{
              height: 40,
              width: "55%",
              borderColor: "gray",
              borderWidth: 1,
              borderRadius: 15,
              backgroundColor: "#fff",
            }}
            onChangeText={(text) => onChangeTextFirstName(text)}
            value={firstName}
          />
        </View>
        <View style={styles.textInput}>
          <Text style={styles.subtitle}>นามสกุล</Text>
          <TextInput
            style={{
              height: 40,
              width: "55%",
              borderColor: "gray",
              borderWidth: 1,
              borderRadius: 15,
              backgroundColor: "#fff",
            }}
            onChangeText={(text) => onChangeTextLastName(text)}
            value={lastName}
          />
        </View>
        <View style={styles.textInput}>
          <Text style={styles.subtitle}>ที่อยู่</Text>
          <TextInput
            style={{
              width: "55%",
              borderColor: "gray",
              borderWidth: 1,
              borderRadius: 15,
              backgroundColor: "#fff",
              display: "flex",
              textAlignVertical: "top",
              height: 100,
            }}
            multiline={true}
            numberOfLines={3}
            onChangeText={(text) => onChangeTextAddress(text)}
            value={address}
          />
        </View>
        <View style={styles.textInput}>
          <Text style={styles.subtitle}>อีเมล์</Text>
          <TextInput
            style={{
              height: 40,
              width: "55%",
              borderColor: "gray",
              borderWidth: 1,
              borderRadius: 15,
              backgroundColor: "#fff",
            }}
            onChangeText={(text) => onChangeTextEmail(text)}
            value={email}
          />
        </View>
        <View style={styles.textInput}>
          <Text style={styles.subtitle}>เบอร์โทร</Text>
          <TextInput
            style={{
              height: 40,
              width: "55%",
              borderColor: "gray",
              borderWidth: 1,
              borderRadius: 15,
              backgroundColor: "#fff",
            }}
            onChangeText={(text) => onChangeTextPhone(text)}
            value={phone}
          />
        </View>
        <View
          style={{
            display: "flex",
            padding: 16,
            flexDirection: "row",
            justifyContent: "space-between",
            backgroundColor: "transparent",
          }}
        >
          <Text style={styles.subtitle}>กรุณาเลือกเพศ</Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              backgroundColor: "transparent",
            }}
          >
            {products.map((val) => {
              return (
                <TouchableOpacity
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginHorizontal: 20,
                  }}
                  key={val.id}
                  onPress={() => radioClick(val.id)}
                >
                  <View
                    style={{
                      height: 24,
                      width: 24,
                      borderRadius: 12,
                      borderWidth: 1,
                      backgroundColor: '#fff',
                      borderColor: "#2F5061",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    {val.id == radioSelected ? (
                      <View
                        style={{
                          height: 12,
                          width: 12,
                          borderRadius: 6,
                          backgroundColor: "#2F5061",
                        }}
                      />
                    ) : null}
                  </View>
                  <Text
                    style={{
                      color: "#2F5061",
                    }}
                  >
                    {val.id === 1 ? "ชาย" : "หญิง"}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
        <View style={styles.buttonInput}>
          <TouchableHighlight
            style={styles.nobordersubmit}
            onPress={onPressRegsiger}
            underlayColor="#fff"
          >
            <Text style={styles.submitText}>สมัครสมาชิก</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.nobordercancke}
            onPress={onPressBack}
            underlayColor="#fff"
          >
            <Text style={styles.submitTextcancke}>ย้อนกลับ</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    </ImageBackground>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     // flex: 1,
//     // justifyContent: "center",
//     height: "100%",
//     backgroundColor: "white",
//     // padding: 24,
//   },
//   header: {
//     height: 100,
//     backgroundColor: "red",
//     padding: 16,
//     display: "flex",
//     justifyContent: "flex-end",
//     alignItems: "center",
//   },
//   textInput: {
//     display: "flex",
//     padding: 16,
//     flexDirection: "row",
//     justifyContent: "space-between",
//   },
//   buttonInput: {
//     display: "flex",
//     padding: 16,
//     flexDirection: "row",
//     justifyContent: "space-around",
//   },
//   title: {
//     fontSize: 20,
//     fontWeight: "bold",
//   },
//   subtitle: {
//     fontSize: 16,
//   },
//   separator: {
//     marginVertical: 30,
//     height: 1,
//     width: "80%",
//   },
// });
