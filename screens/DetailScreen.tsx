import * as React from "react";
import {
  StyleSheet,
  TextInput,
  Button,
  SafeAreaView,
  Image,
  TouchableHighlight,
  ScrollView,
  ImageBackground,
} from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Card } from "react-native-elements";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import firebase from "firebase";
import styles from "../Style";

export default function TabOneScreen({
  navigation,
  route,
}: RootTabScreenProps<"TabOne">) {
  const { action = "travel", amphoe } = route.params;
  const [users, setUsers] = React.useState({});
  const chooseAction = (choose) => {
    console.log("choose", choose);
    navigation.navigate("Comment", {
      ...choose,
    });
  };
  React.useEffect(() => {
    const userRef = firebase.database().ref(action + "/" + amphoe);
    // console.log(firebase.database().ref(action).child(amphoe));

    const OnLoadingListener = userRef.on("value", (snapshot) => {
      // console.log(snapshot.val());

      setUsers([]);

      snapshot.forEach(function (childSnapshot) {
        // console.log(childSnapshot.val());

        setUsers((users) => snapshot.val());
      });
    });

    return () => {
      userRef.off("value", OnLoadingListener);
    };
  }, []);
  const image = require("../assets/images/Asset22-100.jpg");

  const headerText = (text = "") => {
    switch (text) {
      case "kokut":
        return "อำเภอเกาะกูด";
      case "kochang":
        return "อำเภอเกาะช้าง";
      case "mueang":
        return "อำเภอเมือง";
      case "saming":
        return "อำเภอเขาสมิง";
      case "ngop":
        return "อำเภอแหลมงอบ";
      case "khlongyai":
        return "อำเภอคลองใหญ่";
      case "borai":
        return "อำเภอบ่อไร่";
      default:
        return "";
    }
  };

  return (
    <ImageBackground source={image} resizeMode="cover" style={styles.image}>
      <ScrollView>
        <View style={styles.header}>
          <TouchableHighlight onPress={() => navigation.goBack()}>
            <Image
              style={styles.star}
              source={require("../assets/images/previous.png")}
            />
          </TouchableHighlight>
          <Text style={styles.detailtitleheader}>{headerText(amphoe)}</Text>
          <View
            style={{
              height: 20,
              width: 20,
              marginHorizontal: 12,
              backgroundColor: "#4297A0",
            }}
          />
        </View>
        <View style={{ backgroundColor: "transparent" }}>
          {Object.values(users).map((item, index) => (
            <TouchableHighlight
              key={item + "-" + index}
              onPress={() => chooseAction(item)}
            >
              <View style={styles.maincard}>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "flex-start",
                    backgroundColor: "#F4EAE6",
                  }}
                >
                  <Image
                    style={styles.star}
                    source={require("../assets/images/Asset20.png")}
                  />
                  <Text style={styles.detailtitle}>{item["name"]}</Text>
                </View>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#F4EAE6",
                  }}
                >
                  <Image
                    style={styles.card_image}
                    source={{
                      uri: item["image"],
                    }}
                  />
                </View>
              </View>
            </TouchableHighlight>
          ))}
        </View>
      </ScrollView>
    </ImageBackground>
  );
}
// const styles = StyleSheet.create({
//   container: {
//     // flex: 1,
//     // alignItems: "center",
//     // justifyContent: "center",
//     height: "100%",
//     backgroundColor: "white",
//   },
//   title: {
//     fontSize: 20,
//     fontWeight: "bold",
//   },
//   logouttitle: {
//     fontSize: 16,
//     fontWeight: "bold",
//     color: "#4297A0",
//   },
//   separator: {
//     marginVertical: 30,
//     height: 1,
//     width: "80%",
//   },
//   footer: {
//     height: "auto",
//     padding: 16,
//     display: "flex",
//     justifyContent: "center",
//     alignItems: "flex-end",
//     // alignItems: "flex-end",
//     // justifyContent: "center",
//   },
//   maincard: {
//     display: "flex",
//     flexDirection: "column",
//     // justifyContent: "center",
//     // alignItems: "center",
//     backgroundColor: "#F4EAE6",
//     marginVertical: 6,
//     // justifyContent: "center",
//   },
//   textmaincard: {
//     display: "flex",
//     flexDirection: "column",
//     justifyContent: "center",
//     // alignItems: "flex-end",
//     // justifyContent: "center",
//   },
//   tinyLogo: {
//     width: 200,
//     height: 100,
//   },
//   paddingImage: {
//     paddingVertical: 10,
//   },
//   bgviewIvory: {
//     backgroundColor: "#F4EAE6",
//   },
//   card_template: {
//     width: "100%",
//     height: 250,
//     // boxShadow: "10px 10px 17px -12px rgba(0,0,0,0.75)",
//   },
//   card_image: {
//     width: 250,
//     height: 150,
//     margin: 12,
//     borderRadius: 10,
//   },
//   text_container: {
//     position: "absolute",
//     width: 250,
//     height: 30,
//     bottom: 0,
//     padding: 5,
//     backgroundColor: "rgba(0,0,0, 0.3)",
//     borderBottomLeftRadius: 10,
//     borderBottomRightRadius: 10,
//   },
//   card_title: {
//     color: "white",
//   },
//   star: {
//     height: 20,
//     width: 20,
//     marginHorizontal: 12,
//   },
//   header: {
//     height: 100,
//     backgroundColor: "#4297A0",
//     // padding: 16,
//     display: "flex",
//     flexDirection: "row",
//     justifyContent: "space-between",
//     alignItems: "center",
//   },
// });
