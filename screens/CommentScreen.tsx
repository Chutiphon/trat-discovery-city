import * as React from "react";
import {
  StyleSheet,
  TextInput,
  SafeAreaView,
  Image,
  TouchableHighlight,
  ScrollView,
  ImageBackground,
  Alert,
  Linking,
} from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Card } from "react-native-elements";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import firebase from "firebase";
import styles from "../Style";
import {
  Rating,
  AirbnbRating,
  Button,
  Avatar,
  Icon,
} from "react-native-elements";

export default function TabOneScreen({
  navigation,
  route,
}: RootTabScreenProps<"TabOne">) {
  console.log(route.params);
  const [rating, setRating] = React.useState(0);
  const [commnetText, onChangeTextComment] = React.useState("");
  const [usersComment, setUsersComment] = React.useState({});
  const changeRating = (choose = "99") => {
    console.log("choose", choose);
    setRating(choose);
  };

  const {
    detail,
    image,
    name,
    comment,
    foodsuggest,
    moredetail,
    restsuggest,
    star,
    map,
    path,
    linkmap
  } = route.params;
  React.useEffect(() => {
    const userRef = firebase.database().ref(path + "/comment");
    // console.log(firebase.database().ref(action).child(amphoe));

    const OnLoadingListener = userRef.on("value", (snapshot) => {
      // console.log(snapshot.val());

      setUsersComment([]);

      snapshot.forEach(function (childSnapshot) {
        // console.log(childSnapshot.val());

        setUsersComment((comment) => snapshot.val());
      });
    });

    return () => {
      userRef.off("value", OnLoadingListener);
    };
  }, []);
  function generateHexString() {
    return new Date().valueOf();
  }
  const createTwoButtonAlert = (user = {}) =>
    Alert.alert("แสดงความคิดเห็น", "ท่านต้องการแสดงความคิดเห็นหรือไม่", [
      {
        text: "ตกลง",
        onPress: () => {
          const keyGen = generateHexString();
          firebase
            .database()
            .ref(path + "/comment/" + keyGen)
            .set({
              comment: commnetText,
              email: user.email,
              start: rating,
            });
        },
      },
      {
        text: "ยกเลิก",
        onPress: () => {},
      },
    ]);
  const sendLogin = () => {
    console.log("ss");

    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        createTwoButtonAlert(user);
      } else {
        navigation.navigate("Login", {
          path,
          rating,
          commnetText,
        });
        // No user is signed in.
      }
    });
  };
  const imageBg = require("../assets/images/Asset22-100.jpg");

  const logout = () => {};
  return (
    <ImageBackground source={imageBg} resizeMode="cover" style={styles.image}>
      <ScrollView>
        <View style={styles.header}>
          <TouchableHighlight onPress={() => navigation.goBack()}>
            <Image
              style={styles.star}
              source={require("../assets/images/previous.png")}
            />
          </TouchableHighlight>
          <Text style={styles.title}>{name}</Text>
          <View
            style={{
              height: 20,
              width: 20,
              marginHorizontal: 12,
              backgroundColor: "#4297A0",
            }}
          />
        </View>
        <View>
          <Image
            style={{
              height: 200,
              width: "100%",
            }}
            source={{ uri: image }}
          />
        </View>
        <View style={[styles.tab, { padding: 6 }]}>
          <Text>คำบรรยาย</Text>
        </View>
        <View style={{ backgroundColor: "transparent", padding: 6 }}>
          <Text>{detail}</Text>
        </View>
        <View style={[styles.tab, { padding: 6 }]}>
          <Text>แผนที่</Text>
        </View>
        <View>
          <TouchableHighlight
            onPress={() =>
              Linking.openURL(linkmap)
            }
          >
            <Image
              style={{
                height: 200,
                width: "100%",
              }}
              source={{ uri: image }}
            />
          </TouchableHighlight>
        </View>
        <View style={[styles.tab, { padding: 6 }]}>
          <Text>รายละเอียดเพิ่มเติม</Text>
        </View>
        <View style={{ padding: 6, backgroundColor: "transparent" }}>
          <Text>{moredetail}</Text>
        </View>
        <View style={[styles.tab, { padding: 6 }]}>
          <Text>แนะนำเรื่องกิน</Text>
        </View>
        <View style={{ padding: 6, backgroundColor: "transparent" }}>
          <Text>{foodsuggest}</Text>
        </View>
        <View style={[styles.tab, { padding: 6 }]}>
          <Text>แนะนำเรื่องเรื่องที่พัก</Text>
        </View>
        <View style={{ padding: 6, backgroundColor: "transparent" }}>
          <Text>{restsuggest}</Text>
        </View>
        <View style={[styles.tab, { padding: 6 }]}>
          <Text>ให้คะแนน</Text>
        </View>
        <View style={{ padding: 6, backgroundColor: "transparent" }}>
          <Rating
            showRating
            style={{ backgroundColor: "white" }}
            startingValue={rating}
            onFinishRating={changeRating}
            fractions="{1}"
          />
          <TextInput
            style={{
              height: 100,
              borderColor: "gray",
              borderWidth: 1,
              marginVertical: 6,
              textAlignVertical: "top",
              flex: 1,
            }}
            multiline={true}
            numberOfLines={4}
            onChangeText={(text) => onChangeTextComment(text)}
            value={commnetText}
          />
        </View>
        <View
          style={{
            display: "flex",
            alignContent: "center",
            justifyContent: "center",
            alignItems: "center",
            marginVertical: 6,
            backgroundColor: "transparent",
          }}
        >
          <TouchableHighlight
            style={styles.submit}
            onPress={sendLogin}
            underlayColor="#fff"
          >
            <Text style={styles.submitText}>ตกลง</Text>
          </TouchableHighlight>
          {/* <Button title="ตกลง" type="outline" onPress={sendLogin} /> */}
          {/* <Button title="ออกจากระบบ" type="outline" onPress={logout} /> */}
        </View>
        <View
          style={{
            display: "flex",
            alignContent: "center",
            justifyContent: "center",
            marginVertical: 6,
            backgroundColor: "transparent",
          }}
        >
          {Object.values(usersComment).map((item, index) => (
            <TouchableHighlight key={item + "-" + index}>
              <View style={styles.maincard}>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    // alignItems: "center",
                    backgroundColor: "transparent",
                    padding: 12,
                    justifyContent: "flex-start",
                  }}
                >
                  <Text style={styles.subtitle}>{item["comment"]}</Text>
                  <Text style={styles.subtitle}>ผู้ใช้ : {item["email"]}</Text>
                  <Text style={styles.subtitle}>
                    จำนวนดาว : {item["start"]}
                  </Text>
                </View>
              </View>
            </TouchableHighlight>
          ))}
        </View>
      </ScrollView>
    </ImageBackground>
  );
}

// const styles = StyleSheet.create({});
