import * as React from "react";
import {
  StyleSheet,
  TextInput,
  Button,
  SafeAreaView,
  Image,
  TouchableHighlight,
  ScrollView,
  ImageBackground,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import firebase from "firebase";
import { Card, Avatar } from "react-native-elements";
import styles from "../Style";

export default function TabOneScreen({
  navigation,
  route,
}: RootTabScreenProps<"TabOne">) {
  const { action = "travel" } = route.params;
  const chooseAction = (choose) => {
    // console.log("choose", choose);
    navigation.navigate("Detail", {
      action: action,
      amphoe: choose,
    });
  };
  const image = require("../assets/images/Asset22-100.jpg");

  return (
    <ImageBackground source={image} resizeMode="cover" style={styles.image}>
      <ScrollView>
        <View
          style={{
            backgroundColor: "transparent",
          }}
        >
          <View style={styles.modalheader}></View>
          <Image
            style={styles.modaltinyLogo}
            resizeMode="cover"
            source={require("../assets/images/Asset20-100.jpg")}
          />
          <View style={{ backgroundColor: "transparent" }}>
            <TouchableHighlight onPress={() => chooseAction("kokut")}>
              <Card
                containerStyle={{
                  marginHorizontal: 0,
                  marginVertical: 6,
                  backgroundColor: "#F4EAE6",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    backgroundColor: "#F4EAE6",
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    rounded
                    containerStyle={{
                      backgroundColor: "#2F5061",
                      marginHorizontal: 6,
                    }}
                    size={18}
                  />
                  <Text style={styles.titleamphoe}>อำเภอเกาะกูด</Text>
                </View>
              </Card>
            </TouchableHighlight>
          </View>
          <View style={{ backgroundColor: "transparent" }}>
            <TouchableHighlight onPress={() => chooseAction("kochang")}>
              <Card containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    backgroundColor: "#FFF",
                  }}
                >
                  <Avatar
                    rounded
                    containerStyle={{
                      backgroundColor: "#2F5061",
                      marginHorizontal: 6,
                    }}
                    size={18}
                  />
                  <Text style={styles.titleamphoe}>อำเภอเกาะช้าง</Text>
                </View>
              </Card>
            </TouchableHighlight>
          </View>
          <View style={{ backgroundColor: "transparent" }}>
            <TouchableHighlight onPress={() => chooseAction("mueang")}>
              <Card
                containerStyle={{
                  marginHorizontal: 0,
                  marginVertical: 0,
                  backgroundColor: "#F4EAE6",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    backgroundColor: "#F4EAE6",
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    rounded
                    containerStyle={{
                      backgroundColor: "#2F5061",
                      marginHorizontal: 6,
                    }}
                    size={18}
                  />
                  <Text style={styles.titleamphoe}>อำเภอเมือง</Text>
                </View>
              </Card>
            </TouchableHighlight>
          </View>
          <View style={{ backgroundColor: "transparent" }}>
            <TouchableHighlight onPress={() => chooseAction("saming")}>
              <Card containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    backgroundColor: "#FFF",
                  }}
                >
                  <Avatar
                    rounded
                    containerStyle={{
                      backgroundColor: "#2F5061",
                      marginHorizontal: 6,
                    }}
                    size={18}
                  />
                  <Text style={styles.titleamphoe}>อำเภอเขาสมิง</Text>
                </View>
              </Card>
            </TouchableHighlight>
          </View>
          <View style={{ backgroundColor: "transparent" }}>
            <TouchableHighlight onPress={() => chooseAction("ngop")}>
              <Card
                containerStyle={{
                  marginHorizontal: 0,
                  marginVertical: 0,
                  backgroundColor: "#F4EAE6",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    backgroundColor: "#F4EAE6",
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    rounded
                    containerStyle={{
                      backgroundColor: "#2F5061",
                      marginHorizontal: 6,
                    }}
                    size={18}
                  />
                  <Text style={styles.titleamphoe}>อำเภอแหลมงอบ</Text>
                </View>
              </Card>
            </TouchableHighlight>
          </View>
          <View style={{ backgroundColor: "transparent" }}>
            <TouchableHighlight onPress={() => chooseAction("khlongyai")}>
              <Card containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    backgroundColor: "#FFF",
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    rounded
                    containerStyle={{
                      backgroundColor: "#2F5061",
                      marginHorizontal: 6,
                    }}
                    size={18}
                  />
                  <Text style={styles.titleamphoe}>อำเภอคลองใหญ่</Text>
                </View>
              </Card>
            </TouchableHighlight>
          </View>
          <View style={{ backgroundColor: "transparent", marginBottom: 12 }}>
            <TouchableHighlight onPress={() => chooseAction("borai")}>
              <Card
                containerStyle={{
                  marginHorizontal: 0,
                  marginVertical: 0,
                  backgroundColor: "#F4EAE6",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    backgroundColor: "#F4EAE6",
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    rounded
                    containerStyle={{
                      backgroundColor: "#2F5061",
                      marginHorizontal: 6,
                    }}
                    size={18}
                  />
                  <Text style={styles.titleamphoe}>อำเภอบ่อไร่</Text>
                </View>
              </Card>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    </ImageBackground>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     // flex: 1,
//     // alignItems: "center",
//     // justifyContent: "center",
//     height: "100%",
//     backgroundColor: "white",
//   },
//   title: {
//     fontSize: 20,
//     fontWeight: "bold",
//   },
//   logouttitle: {
//     fontSize: 16,
//     fontWeight: "bold",
//     color: "#2F5061",
//   },
//   separator: {
//     marginVertical: 30,
//     height: 1,
//     width: "80%",
//   },
//   footer: {
//     height: "auto",
//     padding: 16,
//     display: "flex",
//     justifyContent: "center",
//     alignItems: "flex-end",
//     // alignItems: "flex-end",
//     // justifyContent: "center",
//   },
//   tinyLogo: {
//     width: "100%",
//     height: 200,
//   },
//   paddingImage: {
//     paddingVertical: 10,
//   },
// });
