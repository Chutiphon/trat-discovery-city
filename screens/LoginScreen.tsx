import * as React from "react";
import {
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  Dimensions,
  TouchableHighlight,
} from "react-native";

import { Button } from "react-native-elements";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import firebase from "firebase";
import styles from "../Style";

export default function TabOneScreen({
  navigation,
}: RootTabScreenProps<"TabOne">) {
  // firebase.auth().onAuthStateChanged(function (user) {
  //   if (user) {
  //     console.log(user.uid);
  //     firebase
  //       .database()
  //       .ref("users")
  //       .child(user.uid)
  //       .once("value")
  //       .then((snapshot) => {
  //         console.log("User data: ", snapshot.val());
  //       });
  //     // console.log(firebase.database().ref("test").get);
  //     // navigation.navigate("Modal");

  //     // navigate("Home");
  //     // await firebase.auth().currentUser.updateProfile(update);
  //     // User is signed in.
  //   } else {
  //     console.log("ddd");

  //     // No user is signed in.
  //   }
  // });
  // const win = Dimensions.get("window");
  const windowWidth = Dimensions.get("window").width;
  const windowHeight = Dimensions.get("window").height;
  // console.log(windowWidth);

  const [email, onChangeTextEmail] = React.useState("");
  const [password, onChangeTextPassword] = React.useState("");
  const onPressLearnMore = () => {
    try {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => navigation.goBack())
        .catch((error) => alert(error));
    } catch (error) {
      alert(error);
    }
  };
  const onPressRegsiger = () => {
    console.log("sss");

    navigation.navigate("Resgister");
  };
  const image = require("../assets/images/Asset22-100.jpg");

  return (
    <View style={styles.logincontainer}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <View
          style={{
            justifyContent: "center",
            backgroundColor: "transparent",
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "transparent",
            }}
          >
            <Image
              style={{
                width: windowWidth - 20,
                height: windowHeight - (windowHeight - 130),
                resizeMode: "contain",
                marginBottom: 12,
              }}
              source={require("../assets/images/Asset23-100.png")}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              backgroundColor: "transparent",
              marginHorizontal: 30,
            }}
          >
            <Image
              style={{
                width: windowWidth - (windowWidth - 80),
                height: windowHeight - (windowHeight - 30),
                resizeMode: "contain",
              }}
              source={require("../assets/images/Asset5.png")}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "transparent",
            }}
          >
            <TextInput
              style={{
                height: 40,
                width: "80%",
                borderColor: "gray",
                borderWidth: 1,
                borderRadius: 15,
                backgroundColor: "#fff",
                fontSize: 18,
              }}
              onChangeText={(text) => onChangeTextEmail(text)}
              value={email}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              backgroundColor: "transparent",
              marginHorizontal: 30,
            }}
          >
            <Image
              style={{
                width: windowWidth - (windowWidth - 80),
                height: windowHeight - (windowHeight - 30),
                resizeMode: "contain",
              }}
              source={require("../assets/images/Asset6.png")}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "transparent",
              marginBottom: 12,
            }}
          >
            <TextInput
              style={{
                height: 40,
                width: "80%",
                borderColor: "gray",
                borderWidth: 1,
                borderRadius: 15,
                backgroundColor: "#fff",
                fontSize: 18,
              }}
              secureTextEntry={true}
              onChangeText={(text) => onChangeTextPassword(text)}
              value={password}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "transparent",
            }}
          >
            <TouchableHighlight
              style={styles.submit}
              onPress={onPressLearnMore}
              underlayColor="#fff"
            >
              <Text style={styles.submitText}>ลงชื่อใช้งาน</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.regitersubmit}
              onPress={onPressRegsiger}
              underlayColor="#fff"
            >
              <Text style={styles.registersubmitText}>สมัครสมาชิก</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}
