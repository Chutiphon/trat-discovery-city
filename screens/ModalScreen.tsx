import * as React from "react";
import {
  StyleSheet,
  TextInput,
  Button,
  SafeAreaView,
  Image,
  TouchableHighlight,
  ScrollView,
  ImageBackground,
  Alert,
} from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import firebase from "firebase";
import styles from "../Style";

export default function TabOneScreen({
  navigation,
}: RootTabScreenProps<"TabOne">) {
  const chooseAction = (choose) => {
    // console.log("choose", choose);
    navigation.navigate("Choose", {
      action: choose,
    });
  };
  const createTwoButtonAlert = () =>
    Alert.alert("ออกจากระบบ", "ท่านต้องการออกจากระบบหรือไม่", [
      { text: "ตกลง", onPress: () => console.log("OK Pressed") },
      { text: "ตกลงlogin", onPress: () => navigation.navigate("Login") },
    ]);
  const onPressLearnMore = () => {
    try {
      firebase.auth().signOut();
      // .then(() => {
      createTwoButtonAlert();
      // })
      // .catch(() => createTwoButtonAlert("no"));
      //   // navigation.navigate("Login");
    } catch (error) {
      alert(error);
    }
  };

  const image = require("../assets/images/Asset22-100.jpg");
  return (
    <ScrollView>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <View style={styles.modalheader}>
          <Text style={styles.modalheadertitle}>หน้าแรก</Text>
        </View>
        <View style={styles.paddingImage}>
          <TouchableHighlight onPress={() => chooseAction("travel")}>
            <Image
              style={styles.modaltinyLogo}
              source={require("../assets/images/Asset11.png")}
            />
          </TouchableHighlight>
        </View>
        <View style={styles.paddingImage}>
          <TouchableHighlight onPress={() => chooseAction("eat")}>
            <Image
              style={styles.modaltinyLogo}
              source={require("../assets/images/Asset12.png")}
            />
          </TouchableHighlight>
        </View>
        <View style={styles.paddingImage}>
          <TouchableHighlight onPress={() => chooseAction("rest")}>
            <Image
              style={styles.modaltinyLogo}
              source={require("../assets/images/Asset13.png")}
            />
          </TouchableHighlight>
        </View>
        <View style={styles.footer}>
          <Text style={styles.logouttitle} onPress={onPressLearnMore}>
            ออกจากระบบ
          </Text>
        </View>
      </ImageBackground>
    </ScrollView>
  );
}
